# Nim front end

This is a [Nim game](https://en.wikipedia.org/wiki/Nim) front end written with Angular 6.

Game server can be found [here](https://gitlab.com/vlntyn/nim).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod` for a production build. The build artifacts will be stored in the `dist/` directory.
