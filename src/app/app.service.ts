import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

    baseUrl: string = 'http://localhost:8080/api';

    startNewGame() {
        return this.http.post(`${this.baseUrl}/game`, {});
    }

    loadGame(gameId: number) {
        return this.http.get(`${this.baseUrl}/game/${gameId}`);
    }

    makeMove(gameId, data) {
        return this.http.post(`${this.baseUrl}/game/${gameId}/move`, data);
    }

    makeAutomatedMove(gameId) {
        return this.http.get(`${this.baseUrl}/game/${gameId}/automove`)
          .pipe(delay(600)); // mimic a delay
    }
}
