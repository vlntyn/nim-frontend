import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { Game } from './model/Game';
import { Move } from './model/Move';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  public currentGame: Game;
  //
  private gameDataSubscription;
  private selection: number[];
  private move: Move;

  constructor(private appService: AppService) {}

  ngOnInit() {
    this.selection = [];
    this.move = new Move();
    // ...
  }

  createNewGame() {
    console.debug('> Create new game');

    this.selection = [];
    this.gameDataSubscription = this.appService.startNewGame()
    .subscribe((newGame: Game) => {
        this.currentGame = newGame;
        console.debug(JSON.stringify(this.currentGame, null, 2));
      }
    );
  }

  makeMove() {
    console.debug('> selection=' + this.selection.length);
    /* ignore empty selection submision */
    if (this.selection.length === 0) {
      return;
    }

    let gameId = this.currentGame.id;
    let playerId = this.currentGame.nextPlayer.id;
    console.debug(`> Make a move, in game: ${gameId} by player: ${playerId}`, gameId, playerId);

    this.move.gameId = gameId;
    this.move.playerId = playerId;
    this.move.moves = this.selection;

    /* send request to the game server */
    this.appService.makeMove(gameId, this.move)
      .subscribe((userUpdatedGame: Game) => {
        this.currentGame = userUpdatedGame;
        this.selection = [];

        // force automated move by a server
        console.debug('> user move registered');
        this.appService.makeAutomatedMove(gameId)
          .subscribe((autoUpdatedGame: Game) => {
            console.debug('> computer move registered');
            this.currentGame = autoUpdatedGame;
          });
      });
  }

  selectElement(index: number): void {
    console.debug('> selecting element at #', index);

    if (this.currentGame['sticks'][index]) {
      console.debug(`> matchstick #${index} is selected.`);

      if (this.selection.indexOf(index) >= 0) {
        this.selection.splice(this.selection.indexOf(index), 1);
        console.debug('> selection exists, removing');
      } else {
        console.debug('> array length=', this.selection.length);
        if (this.selection.length < 3) {
          console.debug('> add index to selection');
          this.selection.push(index);
        }
      }
      console.debug('array: ', this.selection);
    } else {
      console.debug('> selection not possible');
    }
  }

  //
  ngOnDestroy() {
    this.gameDataSubscription.unsubscribe();
  }
}
