import { Move } from '../model/Move';
import { Player } from '../model/Player';

export class Game {
    id: number;
    firstPlayer: Player;
    secondPlayer: Player;
    nextPlayer: Player;
    sticks: boolean[];
    moves: Move[];
    over: boolean;
}
