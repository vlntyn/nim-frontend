export class Move {
    id: number;
    gameId: number;
    playerId: number;
    moves: number[];
}
